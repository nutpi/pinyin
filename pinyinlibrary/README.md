## pinyin

将中文转为拼音。不依赖任何三方库，几KB原生实现，有良好的兼容性，不挑运行环境。

### 示例
```ts


// 引入拼音转换库
import PinyinConverter from '@nutpi/pinyin';

// 创建拼音转换器实例
const converter = new PinyinConverter();

@Entry
@Component
struct Index {
  @State message: string = 'Hello World';

  aboutToAppear(): void {
    // 测试转换功能
    const chineseText = '你好，世界';
    this.message = converter.convert(chineseText);
    console.log(`原始文本: ${chineseText}`);
    // console.log(`转换后的拼音: ${pinyinResult}`); // 输出结果会根据你的数据字典而有所不同
  }

  build() {
    RelativeContainer() {
      Text(this.message)
        .id('HelloWorld')
        .fontSize(50)
        .fontWeight(FontWeight.Bold).onClick(() => {


      })

    }
    .height('100%')
    .width('100%')
  }
}


```
## 使用方法

```sh
ohpm install @nutpi/pinyin
```
## 官网

https://www.nutpi.net/
